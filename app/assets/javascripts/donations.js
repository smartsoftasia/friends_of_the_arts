
$(function(){

  if ($('#donation_monthly').prop("checked") == false) {
    $("#monthly_detail").hide();
    $("#monthly_detail input").prop("disabled",true);
  };

  $("#donation_monthly").on("click", function(){
    $("#monthly_detail").toggle("show");
    $("#monthly_detail input").toggleDisabled();
  });

  $("#new_donation").validate({
    rules: {
      donation_amount: "required",
      card_owner: "required",
      card_no: "required",
      type_of_card: "required",
      exp_month: "required",
      exp_year: "required"
    }
  });
});
