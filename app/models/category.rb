class Category < ActiveRecord::Base
  attr_accessible :name

  has_many :causes, :dependent => :destroy

  validates :name, :presence => true
end
