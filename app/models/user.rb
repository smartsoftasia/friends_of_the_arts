class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  #devise :database_authenticatable, :registerable,
  #       :recoverable, :rememberable, :trackable, :validatable
  devise :timeoutable, :omniauthable, :token_authenticatable, :database_authenticatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name,:facebook_id, :provider, :uid
  # attr_accessible :title, :body
  has_many :donations

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    if !user
      user = User.create(
        #first_name:auth.extra.raw_info.name,
        provider:auth.provider,
        uid:auth.uid,
        email:auth.info.email,
        first_name:auth.info.first_name,
        last_name:auth.info.last_name,
        facebook_id:auth.uid,
        password:Devise.friendly_token[0,20]
      )
    else
      user.update_attributes(email:auth.info.email,first_name:auth.info.first_name,last_name:auth.info.last_name)
    end
    user
  end

  def full_name
    return "#{first_name} #{last_name}"
  end

end
