class Cause < ActiveRecord::Base
  attr_accessible :name, :category_id
  belongs_to :category

  validates :name, :category_id, :presence => true
end
