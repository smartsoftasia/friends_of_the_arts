class Donation < ActiveRecord::Base
  include HTTParty
  base_uri 'https://chabadthailandcoil.clhosting.org'
  
  attr_accessible :user_id, :amount, :cause_id, :anonymous, :nb_month, :monthly, :currency, :pixel_id, :first_name, :last_name
  attr_accessor :first_name, :last_name

  belongs_to :user
  belongs_to :cause

  validates :amount, :numericality => true, :presence => true
  # validates :currency, :pixel_id, :user_id, :cause_id, :presence => true
  # validates :pixel_id, :uniqueness => true
end
