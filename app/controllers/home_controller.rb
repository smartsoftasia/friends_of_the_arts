class HomeController < ApplicationController
#before_filter :authenticate_user!
  require 'open-uri'
  def index
    # sign_out(current_user) if user_signed_in? && !current_user.admin

  	#@img=""
  	#if !current_user.uid.blank?
    #  @img="http://graph.facebook.com/#{current_user.uid}/picture"
    	#end
    @donations = {}
    donations = Donation.order('pixel_id asc').includes(:user, :cause)
    
    pictures = {}
    facebook_ids = donations.map{|e| e.user[:facebook_id]}.compact.join(',')
    unless facebook_ids.blank?
      doc = Nokogiri::HTML(open("https://graph.facebook.com/?ids=#{facebook_ids}&fields=picture&return_ssl_resources=true"))
      pictures = JSON.parse(doc)
    end
    donations.each do |d| 
    	donation = {}
    	donation["user"] = d.anonymous ? "Anonymous" : d.user.full_name
      donation["anonymous"] = d.anonymous
    	donation["cause"] = d.cause.try(:name)
    	donation["facebook_id"] = d.user.facebook_id
      if pictures[d.user.facebook_id] && pictures[d.user.facebook_id]["picture"].present?
        begin
          donation["picture"] = pictures[d.user.facebook_id]["picture"]["data"]["url"]
        rescue
          next
        end
      end
    	@donations[d.pixel_id]=donation
    end
  end
end
