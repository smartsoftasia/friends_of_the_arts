class DonationsController < ApplicationController
  before_filter :current_user_is_admin?, :only => [ :index, :destroy]
  # before_filter :authenticate_user!
  layout 'admin', :only => [ :index, :destroy]

  require 'uri'

  def index
    @donations = Donation.all    
  end

  def new
    if session[:slot].present? || params[:slot].present?
      @donation = Donation.new(:pixel_id => session[:slot] || params[:slot], :first_name => current_user.first_name, :last_name => current_user.last_name)
      session[:slot] = nil # clear session slot
    else
      # flash[:notice] = I18n.t "errors.messages.choose_slot"
      # redirect_to root_path
       @donation = Donation.new
    end
  end

  def pay
    @api = PayPal::SDK::Merchant::API.new
    @do_express_checkout_payment = @api.build_do_express_checkout_payment({
      :DoExpressCheckoutPaymentRequestDetails => {
        :PaymentAction => "Sale",
        :Token => params[:token],
        :PayerID => params[:PayerID],
        :PaymentDetails => [{
          :OrderTotal => {
            :currencyID => "THB",
            :value => session[:total_value]
          }            
        }]
      } 
    })

    # Make API call & get response
    @do_express_checkout_payment_response = @api.do_express_checkout_payment(@do_express_checkout_payment)

    # Access Response

    if @do_express_checkout_payment_response.success?
      if current_user
        @donation = current_user.donations.build(params[:donation])
      else
        user = User.new(params[:donation][:user])
        user.save(validate: false)
        params[:donation].delete("user")
        @donation = user.donations.build(params[:donation])
      end
      
      if @donation.save
        flash[:notice] = I18n.t "notices.messages.thank_you"
        redirect_to root_path
      end
    else
      flash[:notice] = "A transaction cannot be completed. Please contact us"
      redirect_to root_path
    end
  end

  def express_paypal
    
    @donation = Donation.new(params[:donation])
    if @donation.valid?
      amount = params[:donation][:amount] || "1"
      line_items = []
      line_items << {:Name => Cause.find(params[:donation][:cause_id]).name, :Quantity => 1, :Amount => {:currencyID => "THB", :value => amount }, :ItemCategory => "Physical" }
      session[:total_value] = line_items.map {|s| s[:Amount][:value].to_i}.reduce(0, :+)
      
      require 'paypal-sdk-merchant'
      PayPal::SDK.load('config/paypal.yml',  ENV['RACK_ENV'] || 'development')
      @api = PayPal::SDK::Merchant.new
      @set_express_checkout = @api.build_set_express_checkout({
      :SetExpressCheckoutRequestDetails => {
        :ReturnURL => "#{request.protocol}#{request.host}:#{request.port}/donations/pay?#{params.to_query}",
        :CancelURL => "#{request.protocol}#{request.host}:#{request.port}",
        :PaymentDetails => [{
          :OrderTotal => {
            :currencyID => "THB",
            :value => session[:total_value] },
          :ItemTotal => {
            :currencyID => "THB",
            :value => session[:total_value] },
          :PaymentDetailsItem => line_items,
          :PaymentAction => "Sale" }] } })

      # Make API call & get response
      @set_express_checkout_response = @api.set_express_checkout(@set_express_checkout)

      # Access Response
      if @set_express_checkout_response.success?

        # Build request object
        @get_express_checkout_details = @api.build_get_express_checkout_details({:Token => @set_express_checkout_response.Token})

        # Make API call & get response
        @get_express_checkout_details_response = @api.get_express_checkout_details(@get_express_checkout_details)

        # Access Response
        if @get_express_checkout_details_response.success?
          @paypal_url = @api.express_checkout_url(@set_express_checkout_response)
          redirect_to "#{@paypal_url}&useraction=commit"
          # @donation_list = @get_express_checkout_details_response.as_json["ebl:GetExpressCheckoutDetailsResponseDetails"]["ebl:PaymentDetails"][0]["ebl:PaymentDetailsItem"]     
        else
          @get_express_checkout_details_response.Errors
        end
      else
        @set_express_checkout_response.Errors
      end
    else
      flash[:error] = I18n.t "notices.messages.invalid_amount"
      render "new"
    end
  end

  def create


    @donation = current_user.donations.build(params[:donation])
    
    if @donation.save
      # chabad_params = {}
      # if @donation.monthly
      #   chabad_params["monthly amount"] = params[:donation][:amount]
      #   chabad_params["monthly currency"] = params[:donation][:currency]
      #   chabad_params["no of debit's"] = params[:donation][:nb_month]
      # else
      #   chabad_params["one time currency"] = params[:donation][:currency]
      #   chabad_params["One time debit"] = params[:donation][:amount]
      # end
      # chabad_params["Notes"] = "#{params[:to_win]} #{params[:for_the_soul]} #{params["Notes"]}"
      # chabad_params["FormCCOSubmited"] = true
      # new_params = {}
      # params.merge(chabad_params).each do |key, value|
      #   new_params[CGI.escape(key)] = value
      # end
      # Donation.post('/templates/articlecco.asp?AID=841295', :body => new_params)
      flash[:notice] = I18n.t "notices.messages.thank_you"
      redirect_to root_path
    else
      render :action => :new
    end
    # https://chabadthailandcoil.clhosting.org/templates/articlecco.asp?AID=841295
  end

  def destroy
    @donation = Donation.find(params[:id])
    if @donation.destroy
      flash[:notice] = I18n.t "notices.messages.a_donation_was_successfully_deleted"
      redirect_to donations_path
    else
      render :action => :index
    end
  end
end
