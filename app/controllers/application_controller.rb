class ApplicationController < ActionController::Base
  protect_from_forgery

  layout :layout

  private

  def layout
    # only turn it off for login pages:
    is_a?(Devise::SessionsController) ? "sign_in" : "application"
  end

  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

  def after_sign_in_path_for(resource_or_scope)
    new_donation_path
  end  

  def current_user_is_admin?
    unless current_user.try(:admin?)
      flash[:warning] = I18n.t 'you_are_not_allowed_to_access_this_page'
      redirect_to root_path
    end
  end

end
