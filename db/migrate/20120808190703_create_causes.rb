class CreateCauses < ActiveRecord::Migration
  def change
    create_table :causes do |t|
      t.string :name
      t.integer :category_id
      t.timestamps
    end
  end
end
