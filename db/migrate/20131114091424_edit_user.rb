class EditUser < ActiveRecord::Migration
  def up
    change_column :users, :encrypted_password, :string, :null => true
    change_column :users, :facebook_id, :string, :null => true
  end

  def down
  end
end
