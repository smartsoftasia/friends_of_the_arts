class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.integer :user_id
      t.float :amount
      t.integer :cause_id
      t.boolean :anonymous
      t.integer :nb_month
      t.boolean :monthly
      t.string :currency
      t.integer :pixel_id
      t.timestamps
    end
  end
end
