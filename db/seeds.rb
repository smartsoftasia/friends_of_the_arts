# encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

categories = Category.create([
    {name: 'לאכול'},
    {name: 'להתפלל'},
    {name: 'להתחבר'}
  ])

Cause.create([
    {name: 'סעודות שבת וחג', category_id: categories[0].id},
    {name: 'עונג שבת', category_id: categories[0].id},
    {name: 'מסעדה כשרה ומסובסדת', category_id: categories[0].id},
    {name: 'שיעורים ולימודי יהדות', category_id: categories[1].id},
    {name: 'עמדות הנחת תפילין', category_id: categories[1].id},
    {name: 'חלוקת נרות שבת', category_id: categories[1].id},
    {name: 'סדנאות מודעות וזוגיות', category_id: categories[2].id},
    {name: 'חיבור בין כל חלקי העם', category_id: categories[2].id},
    {name: 'עזרה הומניטארית', category_id: categories[2].id}
  ])

user = User.new(:email => "admin@smartsoftasia.com", first_name: 'admin', last_name: 'admin', facebook_id: "1518242909", provider: "facebook", uid: "1518242909", password: "password", password_confirmation: "password")
user.save(validate: false)
user.update_attribute(:admin, true)